variable "region" {
  description = "Region of the AWS VPC"
  type        = string
}

variable "aws_profile" {
  description = "The AWS profile we should be using to interact with Terraform"
  type        = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  default     = "serverless-typescript"
  type        = string
}
