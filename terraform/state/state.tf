provider "aws" {
  profile = var.aws_profile
  region  = var.region
}

resource "aws_s3_bucket" "state_bucket" {
  bucket = "terraform-state-bucket-${var.project_name}"
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-lock-table-${var.project_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}
