terraform {
  backend "s3" {
    # Cannot use variables
    bucket = "terraform-state-bucket-serverless-typescript"
    key    = "tfstate/terraform.tfstate"
    # Cannot use variables
    region = "us-east-1"
    # Cannot use variables
    dynamodb_table = "terraform-lock-table-serverless-typescript"
  }
}

provider "aws" {
  profile = var.aws_profile
  region  = var.region
}

module "dev-environment" {
  source             = "../modules/environment"
  environment_name = "dev"
  project_name = var.project_name
  main_domain_name = var.dev_domain_name
  parent_domain_name = var.dev_parent_domain_name
}

module "prod-environment" {
  source             = "../modules/environment"
  environment_name = "prod"
  project_name = var.project_name
  main_domain_name = var.prod_domain_name
  parent_domain_name = var.prod_parent_domain_name
}