variable "region" {
  description = "Region of the AWS VPC"
  type        = string
}

variable "aws_profile" {
  description = "The AWS profile we should be using to interact with Terraform"
  type        = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  default     = "serverless-typescript"
  type        = string
}

variable "dev_domain_name" {
  description = "The domain to use for an SSL certificate for the dev environment. Main domain must be registered in Route 53 (example: subdomain.mydomain.com)"
  type        = string
}

variable "dev_parent_domain_name" {
  description = "If a subdomain is being used (subdomain.mydomain.com) then this has to be set to the parent domain (mydomain.com) for the dev environment"
  type        = string
  default     = ""
}

variable "prod_domain_name" {
  description = "The domain to use for an SSL certificate for the prod environment. Main domain must be registered in Route 53 (example: subdomain.mydomain.com)"
  type        = string
}

variable "prod_parent_domain_name" {
  description = "If a subdomain is being used (subdomain.mydomain.com) then this has to be set to the parent domain (mydomain.com) for the prod environment"
  type        = string
  default     = ""
}