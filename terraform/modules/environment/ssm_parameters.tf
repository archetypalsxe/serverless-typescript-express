resource "aws_ssm_parameter" "domain_name" {
  name  = "${var.project_name}-${var.environment_name}-domain"
  type  = "String"
  value = var.main_domain_name
}