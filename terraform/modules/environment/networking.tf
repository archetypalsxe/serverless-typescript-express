locals {
  zone_id = var.parent_domain_name == "" || var.parent_domain_name == var.main_domain_name ? data.aws_route53_zone.existing_route53_zone[0].zone_id : aws_route53_zone.route53_zone[0].zone_id
}

resource "aws_acm_certificate" "ssl_cert" {
  domain_name       = var.main_domain_name
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "existing_route53_zone" {
  count = var.parent_domain_name == "" || var.parent_domain_name == var.main_domain_name ? 1 : 0
  name  = var.main_domain_name
}

resource "aws_route53_zone" "route53_zone" {
  count = var.parent_domain_name == "" || var.parent_domain_name == var.main_domain_name ? 0 : 1
  name  = var.main_domain_name
}

# This is used for situations where the domain_name is a sub domain
data "aws_route53_zone" "main_zone" {
  count = var.parent_domain_name == "" || var.parent_domain_name == var.main_domain_name ? 0 : 1
  name  = var.parent_domain_name
}

# This is used for situations where the domain_name is a sub domain
resource "aws_route53_record" "domain_namespace" {
  count   = var.parent_domain_name == "" || var.parent_domain_name == var.main_domain_name ? 0 : 1
  zone_id = data.aws_route53_zone.main_zone[0].zone_id
  name    = var.main_domain_name
  type    = "NS"
  ttl     = "30"
  records = aws_route53_zone.route53_zone[0].name_servers
}

# Validate the SSL Certificate
resource "aws_route53_record" "domain" {
  for_each = {
    for dvo in aws_acm_certificate.ssl_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = local.zone_id
}

resource "aws_acm_certificate_validation" "acm_validation" {
  certificate_arn         = aws_acm_certificate.ssl_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.domain : record.fqdn]

  timeouts {
    create = "30m"
  }
}