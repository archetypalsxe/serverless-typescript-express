variable "environment_name" {
  description = "Unique identifier so that we can deploy resources named similarly in the same region"
  type = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  default     = "serverless-typescript"
  type        = string
}

variable "main_domain_name" {
  description = "The domain to use for an SSL certificate. Main domain must be registered in Route 53 (example: subdomain.mydomain.com)"
  type        = string
}

variable "parent_domain_name" {
  description = "If a subdomain is being used (subdomain.mydomain.com) then this has to be set to the parent domain (mydomain.com)"
  type        = string
  default     = ""
}