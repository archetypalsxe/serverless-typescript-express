import type { AWS } from '@serverless/typescript';

import hello from '@functions/hello';
import bye from '@functions/bye';

const serverlessConfiguration: AWS = {
  service: 'serverless',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild', 'serverless-offline', 'serverless-domain-manager' ],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
  },
  // import the function via paths
  functions: { hello, bye },
  package: { individually: true },
  custom: {
    customDomain: {
      basePath: '',
      createRoute53Record: true,
      // This may have to be modified if the project_name is changed
      domainName: "${ssm:serverless-typescript-${sls:stage}-domain}",
      stage: "${sls:stage}",
    },
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
  },
};

module.exports = serverlessConfiguration;
