# Serverless Typescript Express

Creating an example of a Typescript/Express API deployed with the Serverless framework

## Terraform

### Initial Setup
* If you're using `tfenv`
  * `tfenv install latest`
    * `1.2.2` at the time of running this
  * `tfenv use latest`
* From the `terraform/state` directory:
  * `terraform init`
  * `terraform apply`
    * This will setup a shared place (S3 and DynamoDB) in AWS for Terraform state

### Deploying
* From the `terraform/main` directory:
  * Will deploy:
    * ACM Certificate for handing HTTPS requests
  * `terraform init`
  * `terraform apply`

### Removing
* **The serverless app should be deleted before running these commands**
  * Otherwise, removal will fail for some resources due to being in use
  * Will have to recreate all the resources in order to successfully remove the serverless app as well
* From the `terraform/main` directory:
  * `terraform destroy`
* From the `terraform/state` directory:
  * `terraform destroy`

## Serverless

### Pre Requirements
* Make sure you have the Serverless framework and the AWS SDK installed
  * `sudo npm i -g serverless aws-sdk`
* From the serverless directory:
  * `npm install`

### Running Locally
* From the `serverless` directory:
  * `serverless offline start`

#### Testing
* Sending a sample request (stolen from the serverless README):
  * Does not require the serverless offline to gbe running... somehow
    * `npx serverless invoke local -f hello --path src/functions/hello/mock.json`
    * `npx serverless invoke local -f bye`
  * Serverless offline has to be running:
    * `curl --location --request POST 'localhost:3000/dev/hello' --header 'Content-Type: application/json' --data-raw '{"name": "yourName"}'`
    * `curl localhost:3000/dev/bye`

### Deploying
* Terraform should be ran previously
* Setup Serverless Creds:
  * `serverless`
    * Make sure to enter AWS access keys
* Set `AWS_PROFILE` if necessary:
  * `export AWS_PROFILE="${aws_profile:?}"`
* The stage variable is only needed if not in dev
* Deploy the domain:
  * `serverless create_domain --stage ${stage:?}`
* Deploy:
  * `serverless deploy --stage ${stage:?}`
    * You can check progress by watching the CloudFormation Stacks dashboard in the AWS console

#### Testing
* Set the endpoint that is generated to an `$endpoint` variable:
  * `endpoint=https://xxxxx12345.execute-api.us-east-1.amazonaws.com/dev/`
  * `curl "${endpoint:?}/bye"`
  * `curl --location --request POST "${endpoint:?}/hello" --header 'Content-Type: application/json' --data-raw '{"name": "yourName"}'`
* With domain set to the domain being used (`subdomain.mydomain.com`)
  * `curl https://${domain:?}/bye`
  * `curl --location https://"${domain:?}"/hello --request POST --header 'Content-Type: application/json' --data-raw '{"name": "yourName"}'`

### Deleting (Undeploying...?)
* The stage variable is only needed if not in dev
* `serverless remove --stage ${stage:?}`
* `serverless delete_domain --stage ${stage:?}`

### How It Was Set Up
Users shouldn't have to do any of this, but in case it's helpful...

* Creating serverless template:
  * `serverless create --template aws-nodejs-typescript`
* Installing `serverless-offline` to run locally
  * `npm install -D serverless-offline`
* Installing express and required packages:
  * `npm install aws-lambda serverless-http express @types/express`
* Adding the Serverless domain manager
  * `npm install -D serverless-domain-manager`

## Resources Used:
* [Serverless Article on Setting Up Domains](https://www.serverless.com/blog/serverless-api-gateway-domain/)
* [Docs for Serverless Parameters](https://www.serverless.com/framework/docs/providers/aws/guide/variables#referencing-parameters)
* [Karthik S Kumar's Article](https://javascript.plainenglish.io/build-your-first-serverless-app-with-aws-lambda-api-gateway-express-and-typescript-2020-4841f54514eb)
  * This article is quite out of date as the template generator has changed significantly from when the article was published (July 2020)
    * Biggest difference is that Typescript is now installed by default rather than having to be installed afterwards
